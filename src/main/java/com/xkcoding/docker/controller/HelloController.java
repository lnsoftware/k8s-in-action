package com.xkcoding.docker.controller;

import java.io.File;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Value;

/**
 * <p>
 * Hello Controller
 * </p>
 *
 * @package: com.xkcoding.docker.controller
 * @description: Hello Controller
 * @author: yangkai.shen
 * @date: Created in 2018-11-29 14:58
 * @copyright: Copyright (c) 2018
 * @version: V1.0
 * @modified: yangkai.shen
 */
@RestController
@RequestMapping
public class HelloController {
	
	@Value(value = "${user.password}")
    private String password;
	
    @GetMapping
    public String hello() {
        return "Hello, DevOps "+ password +" Docker!";
    }
	
	@GetMapping("create")
	public String create() throws Exception {
		File file = new File("/usr/config/index.html");
		if (!file.exists()) {
			file.createNewFile();
			return "create file success";
		}
		return "file exist";
	}
	
}
