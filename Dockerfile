# 基础镜像
FROM lnsoftware/openjdk8:2.0.0

# 作者信息
MAINTAINER "Yangkai.Shen 237497819@qq.com"

# 修改apt镜像为国内镜像
RUN  sed -i s@/archive.ubuntu.com/@/mirrors.aliyun.com/@g /etc/apt/sources.list

# 安装必要工具
RUN apt-get update -y \
    && apt-get install --assume-yes apt-utils \
	&& apt-get install -y curl \
	&& apt-get install -y vim \
	&& apt-get install -y net-tools \
	&& apt-get install -y inetutils-ping \
	&& apt-get install -y nfs-kernel-server \
	&& apt-get install -y nfs-common 

# 添加一个存储空间
VOLUME /tmp

# 暴露8080端口
EXPOSE 8080

# 添加变量，如果使用dockerfile-maven-plugin，则会自动替换这里的变量内容
ARG JAR_FILE=target/spring-boot-demo-docker.jar

# 往容器中添加jar包
ADD ${JAR_FILE} app.jar

# 启动镜像自动运行程序
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/urandom","-jar","/app.jar"]